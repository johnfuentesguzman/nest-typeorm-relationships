# README #

Nest JS Project to develop a crud system using TypeOrm

### What will I learn? ###

* Task Controllers
* Task Services
* Validation Pipeline
* Task Entinty
* authentication: AuthService and JWT

* Using pipes  https://github.com/typestack/class-validator#validations-decotators

* using repositories: http://typeorm.delightful.studio/classes/_repository_repository_.repository.html

* creating CUSTOM repostiroies: https://github.com/typeorm/typeorm/blob/master/docs/custom-repository.md


### Installation ###
 - npm i -g @nestjs/cli

 ### creating module  using nest CLI ###
 - nest g module [name]

  ### creating controller using nest CLI ###
  - nest g controller [name] ---> including unit test file (.spec)
  - nest g controller [name] --no-spec ---> NOT includes unit test file (.spec)

  ### creating service using nest CLI ###
  - nest g service [name] ---> including unit test file (.spec)
  - nest g service [name] --no-spec ---> NOT includes unit test file (.spec)

  ### Install pipes
  - npm i class-validator class-transformer --save

  ### Install typeOrm and the postgress driver [pg]
  - npm i @nestjs/typeorm typeorm pg
  

  


![API Endpoints](https://bitbucket.org/johnfuentesguzman/nest-crud-typeorm/raw/40948d73272e0a32fb7826e2b1fc295200fe60f0/ReadmeFIles/Api%20Endpoints.png)
![Auth Endpoints](https://bitbucket.org/johnfuentesguzman/nest-crud-typeorm/raw/40948d73272e0a32fb7826e2b1fc295200fe60f0/ReadmeFIles/Auth%20endpoints.png)  

### source code filters
https://github.com/arielweinberger/nestjs-course-task-management/tree/persistence/10-refactor-both-getting-all-tasks-and-filters

## source code persitence using postgress
https://github.com/arielweinberger/nestjs-course-task-management/tree/persistence/10-refactor-both-getting-all-tasks-and-filters


# How TypeOrm works # 
### you must create a config file : ormconfig.json


- {
    "type": "postgres",
    "host": "localhost",
    "port": 5432,
    "username": "postgres",
    "password": "admin",
    "database": "relationsships",
    "entities": ["./src/entities/*.entity.ts"],
    "synchronize": true,
    "migrations": ["./src/migration/*.ts"],
    "cli": {
      "migrationsDir": "./src/migration/"
    }
  }
- migrationsDir is the place where the migration file with sql query will be saved

### Generate migration command (package.json) to generate the migration file  in "./src/migration/"
- npm run typeorm:generate [migrationName]

### Run migrations command  (package.json) saved in "./src/migration/"
-npm run  typeorm:run