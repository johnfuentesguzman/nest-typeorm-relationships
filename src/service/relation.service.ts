import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RelationsDto } from 'src/dto/relations-data.dto';
import { PhotosRepository } from '../repository/photos.repository';
import { UserRepository } from '../repository/user.repository';

@Injectable()
export class RelationsService {
    constructor( 
        @InjectRepository(PhotosRepository)
        @InjectRepository(UserRepository)

        private photosRepository: PhotosRepository,
        private userRepository: UserRepository
    ){}
    
      async saveRelations(data: RelationsDto): Promise<any> {
        const photo = await this.photosRepository.createPhoto(data);
        let user = null;

        if(photo){
            console.log(JSON.stringify(photo))
            user = await this.userRepository.createUser(data, photo);
        }
        if(photo && user){
            return  {
                status: 200,
                photo,
                user
            }
        } else {
            throw new HttpException('Cannot add data', HttpStatus.BAD_REQUEST);
        }
      }

      async saveRelationsOpposite(data: RelationsDto): Promise<any> {
        const user = await this.userRepository.createUserFirst(data)
        let photo = null;

        if(user){
            console.log(JSON.stringify(photo))
            photo = await this.photosRepository.createPhotoToEnd(data, user);
        }
        if(photo && user){
            return  {
                status: 200,
                photo,
                user
            }
        } else {
            throw new HttpException('Cannot add data', HttpStatus.BAD_REQUEST);
        }
      }
      async saveRelationsCascade(data: RelationsDto): Promise<any> {
        const saveData = await this.userRepository.save(data)
        if(saveData){
            return  {
                status: 200,
                saveData
            }
        } else {
            throw new HttpException('Cannot add data', HttpStatus.BAD_REQUEST);
        }
      }
}
