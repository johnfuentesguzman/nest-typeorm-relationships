import { User } from '../entities/user.entity';
import { EntityRepository, Repository } from 'typeorm';
import { RelationsDto } from 'src/dto/relations-data.dto';
import {Photo} from "../entities/photo.entity";


@EntityRepository(User)
export class UserRepository extends Repository<User> {

  async createUser(data: RelationsDto, photosaved: Photo): Promise<User> {
    const { name, photos } = data;

    const newUser = new User();
    newUser.name = name;
    newUser.photos = [photosaved]
    await newUser.save();
    return newUser;
  }

  async createUserFirst(data: RelationsDto): Promise<User> {
    const { name } = data;

    const newUser = new User();
    newUser.name = name;
    await newUser.save();
    return newUser;
  }
}