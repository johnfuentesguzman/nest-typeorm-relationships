
import { Photo } from '../entities/photo.entity';
import { EntityRepository, Repository } from 'typeorm';
import { RelationsDto } from 'src/dto/relations-data.dto';
import {User} from "../entities/user.entity";

@EntityRepository(Photo)
export class PhotosRepository extends Repository<Photo> {

  async createPhoto(data: RelationsDto): Promise<Photo> {
    const newPhoto = new Photo();
    newPhoto.url = data.photos[0].url;
    await newPhoto.save();
    return newPhoto;
  }


  async createPhotoToEnd( data: RelationsDto, userSaved: User ): Promise<Photo> {
    const newPhoto = new Photo();
    newPhoto.url = data.photos[0].url;
    newPhoto.user = userSaved;
    await newPhoto.save();
    return newPhoto;
  }
}