export interface Photo {
    url: string;
}

export interface RelationInterface {
    name: string;
    photos: Photo[];
}