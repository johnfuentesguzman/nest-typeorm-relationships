import { RelationsDto } from '../dto/relations-data.dto';
import { Body, Controller, Post } from '@nestjs/common';;
import { RelationsService } from 'src/service/relation.service';

@Controller('relations')
export class RelationsController {
  constructor(private relationService: RelationsService) { }

  //**** test of relations */
  @Post('')
  setReleations(
    @Body() setRelationsData: RelationsDto
  ): Promise<any> {
    return this.relationService.saveRelations(setRelationsData);
  }

  //**** test of relations saving user fist and then photos with user payload */
  @Post('/oppossite')
  setReleationsOpossite(
    @Body() setRelationsData: RelationsDto
  ): Promise<any> {
    return this.relationService.saveRelationsOpposite(setRelationsData);
  }


    //**** test of relations saving using CASCADE = saving user and photo with just one save method */
    @Post('/cascade')
    setReleationsCascade(
      @Body() setRelationsData: RelationsDto
    ): Promise<any> {
      return this.relationService.saveRelationsCascade(setRelationsData);
    }
  
}
