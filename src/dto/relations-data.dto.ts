import { Photo } from '../interface/relation-data.interface';
import { IsNotEmpty } from 'class-validator';

export class RelationsDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  photos:  Photo[]
}

