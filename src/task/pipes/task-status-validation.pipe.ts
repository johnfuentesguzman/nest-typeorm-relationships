
import { ArgumentMetadata, BadRequestException, PipeTransform } from "@nestjs/common";
import { TaskStatus } from "../task-status.enum";

export class TaskStatusValidationPipe implements PipeTransform {
    readonly allowedStatus = [
        TaskStatus.DONE,
        TaskStatus.IN_PROGRESS,
        TaskStatus.OPEN
    ]

    transform(parametersValue: any, metadata: ArgumentMetadata){
        if(!this.istatusValid(parametersValue)){
            throw new BadRequestException(`${parametersValue} is not a valid Status`)
        }
         
        return parametersValue.toUppercase()
    }
    
    private istatusValid(status: any){
        const index = this.allowedStatus.indexOf(status)
        return index !== -1; // if the status   no exist as  valid in the request parameters, it will return fallse
     }
}