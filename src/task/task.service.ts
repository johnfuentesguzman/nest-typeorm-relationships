import { TaskRepository } from './task.repository';
import { CreateTaskDto } from './dto/create-task.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { GetDataFilterDto } from './dto/get-task-filter.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from '../entities/task.entity';
import { TaskStatus } from './task-status.enum';

@Injectable()
export class TaskService {
    constructor( 
        @InjectRepository(TaskRepository)
        private taskRepository: TaskRepository
    ){}

    async getTasks(filterDto: GetDataFilterDto): Promise<Task[]> {
        return this.taskRepository.getTasks(filterDto);
      }
    
      async getTaskById(id: number): Promise<Task> {
        // findone is a default function into EntityRepository/typeorm class (https://typeorm.io/#/)
        const found = await this.taskRepository.findOne(id);
    
        if (!found) {
          throw new NotFoundException(`Task with ID "${id}" not found`);
        }
    
        return found;
      }
    
      async createTask(createTaskDto: CreateTaskDto): Promise<Task> {
        return this.taskRepository.createTask(createTaskDto);
      }
    
      async deleteTask(id: number): Promise<any> {
        // delete is a default function into EntityRepository/typeorm class (https://typeorm.io/#/)
        const result = await this.taskRepository.delete(id);
    
        if (result.affected === 0) {
          throw new NotFoundException(`Task with ID "${id}" not found`);
        }else{
          return {statusCode: 200, message: `${id} delete successfully`}
        }
      }
    
      async updateTaskStatus(id: number, status: TaskStatus): Promise<Task> {
        const task = await this.getTaskById(id);
        task.status = status;
        await task.save();
        return task;
      }
}
