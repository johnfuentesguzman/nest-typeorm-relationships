import { IsIn, IsNotEmpty, IsOptional } from 'class-validator';
import { TaskStatus } from '../task-status.enum';

export class GetDataFilterDto {
    @IsOptional()
    @IsIn([TaskStatus.OPEN,  TaskStatus.IN_PROGRESS, TaskStatus.DONE]) // one of these 3 status must have as paramter
    status: TaskStatus

    @IsOptional()
    @IsNotEmpty()
    search: string
}