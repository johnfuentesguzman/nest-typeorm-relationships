import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';
import { TaskRepository } from './task.repository';
import { RelationsService } from 'src/service/relation.service';
import { PhotosRepository } from 'src/repository/photos.repository';
import { UserRepository } from 'src/repository/user.repository';
import { RelationsController } from 'src/controller/relations.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([TaskRepository, PhotosRepository, UserRepository])
  ],
  controllers: [TaskController, RelationsController],
  providers: [TaskService, RelationsService]
})
export class TaskModule {}
