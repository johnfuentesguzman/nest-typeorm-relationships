import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'admin',
    database: 'relationsships',
    entities: [__dirname + '/../**/*.entity.{js,ts}'], // it will take all file with .entity
    synchronize: true,
    migrations: ["./migrations/*.ts"]
};